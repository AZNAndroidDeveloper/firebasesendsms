package uz.app.loginscreen.data.model

import java.io.Serializable

data class User (
    val userName:String,
    val userPassword:String,
    val userEmail:String,
    val userPhone:String
):Serializable{
    constructor():this("","","","")
}