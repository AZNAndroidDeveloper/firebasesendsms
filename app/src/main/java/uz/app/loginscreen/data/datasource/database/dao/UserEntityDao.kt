package uz.app.loginscreen.data.datasource.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import uz.app.loginscreen.data.model.UserEntity

@Dao
abstract class UserEntityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(vararg users: UserEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun update(vararg users:UserEntity)
}