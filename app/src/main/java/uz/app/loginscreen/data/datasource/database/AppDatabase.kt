package uz.app.loginscreen.data.datasource.database

import android.content.Context
import androidx.room.*
import uz.app.loginscreen.data.datasource.database.dao.UserEntityDao
import uz.app.loginscreen.data.model.UserEntity

@Database(
    entities = [
        UserEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract val userEntityDao: UserEntityDao

    companion object {
        private const val DATABASE_NAME: String = "user_database"
        fun create(context: Context): AppDatabase = Room.databaseBuilder(
            context, AppDatabase::class.java,
            DATABASE_NAME
        ).allowMainThreadQueries()
            .build()

    }

}