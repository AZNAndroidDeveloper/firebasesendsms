
package uz.app.loginscreen.presentation.smsCodeFragment

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import uz.app.loginscreen.R
import uz.app.loginscreen.databinding.FragmentSmsBinding
import uz.app.loginscreen.presentation.singInFragment.SingInFragmentArgs

class SmsFragment : Fragment(R.layout.fragment_sms) {
    private lateinit var binding:FragmentSmsBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSmsBinding.bind(view)
        with(binding) {
            btnEnter.setOnClickListener {
                if (etCode.text!!.length == 6) {
                    findNavController().navigate(SmsFragmentDirections.actionSmsFragmentToUserFragment())
                }else{
                    tvCode.error = "Kodingiz hatto"
                }
            }
        }

    }
    private val fragmentArgs by lazy {
        navArgs<SmsFragmentArgs>().value
    }

}