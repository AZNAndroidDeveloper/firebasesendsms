package uz.app.loginscreen.presentation.usersFragment.useraAdapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import uz.app.loginscreen.R
import uz.app.loginscreen.data.model.User
import uz.app.loginscreen.databinding.UserItemBinding as Binding

class UserAdapter() : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {
var  elements = mutableListOf<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(Binding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val element = elements[position]
        holder.username.text = element.userName
        holder.email.text = element.userEmail
        holder.password.text = element.userPassword
        holder.phone.text = element.userPhone

    }

    fun setData(element: MutableList<User>) {
  elements.apply { clear() ; addAll(element) }
        notifyDataSetChanged()

    }

    inner class UserViewHolder(binding:Binding) : RecyclerView.ViewHolder(binding.root) {
        var username = binding.tvUsername

        var password = binding.passwordTv
        var phone = binding.tvPhone
        var email= binding.emailTv

    }

}