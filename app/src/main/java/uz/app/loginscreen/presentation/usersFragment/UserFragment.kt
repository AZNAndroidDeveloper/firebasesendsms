package uz.app.loginscreen.presentation.usersFragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.google.firebase.database.*
import uz.app.loginscreen.R
import uz.app.loginscreen.data.model.User
import uz.app.loginscreen.databinding.FragmentSingInBinding
import uz.app.loginscreen.databinding.FragmentUserBinding
import uz.app.loginscreen.presentation.usersFragment.useraAdapter.UserAdapter
import java.util.regex.Pattern

class UserFragment : Fragment(R.layout.fragment_user),
    NavigationView.OnNavigationItemSelectedListener, Toolbar.OnMenuItemClickListener {
    private lateinit var binding: FragmentUserBinding
    private lateinit var database: FirebaseDatabase
    private lateinit var referance: DatabaseReference
    private lateinit var userList: MutableList<User>
    private lateinit var adapter2: UserAdapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentUserBinding.bind(view)
        getShared(true)

        database = FirebaseDatabase.getInstance()
        referance = database.getReference("Users")
        adapter2 = UserAdapter()
        userList = arrayListOf()
        getData()
        val toggle: ActionBarDrawerToggle = ActionBarDrawerToggle(
            requireActivity(), binding.drawerLayout, binding.toolbar,
            R.string.open, R.string.close
        )
        with(binding) {
            drawerLayout.addDrawerListener(toggle)
            navView.setNavigationItemSelectedListener(this@UserFragment)
        }
        toggle.syncState()


    }

    private fun getData() {
        referance.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                for (user in snapshot.children) {
                    val model = user.getValue(User::class.java)
                    userList.add(model as User)

                    adapter2.setData(userList)
                }

            }

        })

        with(binding) {
            recycleView.apply {
                layoutManager =
                    LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
                adapter = adapter2

            }

        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                Toast.makeText(requireContext(), "HomePage", Toast.LENGTH_SHORT).show()
            }
            R.id.setting -> {
                Toast.makeText(requireContext(), "Setting", Toast.LENGTH_SHORT).show()
            }
            R.id.log_out -> {
                Toast.makeText(requireContext(), "Logout", Toast.LENGTH_SHORT).show()
                getShared(false)
                findNavController().navigate(UserFragmentDirections.actionUserFragmentToSingInFragment())
            }
        }
        with(binding) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        return true
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.refresh -> {
                getData()
            }
        }
        return true
    }

    private fun getShared(ishave: Boolean) {
        val preferences = requireActivity().getSharedPreferences("context", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putBoolean("isHave", ishave)
        editor.apply()
    }

}
