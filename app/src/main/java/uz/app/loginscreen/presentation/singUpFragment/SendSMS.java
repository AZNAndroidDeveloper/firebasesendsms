package uz.app.loginscreen.presentation.singUpFragment;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Random;

public class SendSMS {
    public String sendSms(int telNumber) {
        try {
            // Construct data
            String apiKey = "apikey=" + "Eq3cu1vOex4-6kDU8Pqzko42ie6e8ZfvPxWkZmmkjV";
            Random random = new Random();
            int number = random.nextInt(99999);
            String message = "&message=" + "Code" + number;
            String sender = "&sender=" + "SoftData";
            String numbers = "&numbers=" + telNumber;

            // Send data
            HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
            String data = apiKey + numbers + message + sender;
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuffer.append(line);
            }
            rd.close();

            return stringBuffer.toString();
        } catch (Exception e) {
            System.out.println("Error SMS "+e);
            return "Error "+e;
        }
    }
}
