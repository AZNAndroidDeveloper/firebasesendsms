package uz.app.loginscreen.presentation.singInFragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.firebase.database.*
import uz.app.loginscreen.R
import uz.app.loginscreen.data.model.User
import uz.app.loginscreen.databinding.FragmentSingInBinding

class SingInFragment : Fragment(R.layout.fragment_sing_in){
    private lateinit var binding: FragmentSingInBinding
    private lateinit var database: FirebaseDatabase
    val userList = mutableListOf<User>()
    var isHave = false
    var count = 0
    private lateinit var referance: DatabaseReference
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val preferences = requireActivity().getSharedPreferences("context",Context.MODE_PRIVATE)
        var isLogged = preferences.getBoolean("isHave",false)
        if (isLogged){
            findNavController().navigate(SingInFragmentDirections.actionSingInFragmentToUserFragment())
        }
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSingInBinding.bind(view)
        database = FirebaseDatabase.getInstance()
        referance = database.getReference("Users")

        val dataList = getData()
        Log.d("dataList", "onViewCreated: $dataList")
        with(binding) {
            forgotTv.setOnClickListener {
                findNavController().navigate(SingInFragmentDirections.actionSingInFragmentToSingUpFragment())
            }
            login.setOnClickListener {
                val username = etUsername.text.toString().trim()
                val password = etPassword.text.toString().trim()
                if (username.isNotEmpty() && password.isNotEmpty()) {

                    for (i in dataList.indices) {
                        if (username == dataList[i].userName && password == dataList[i].userPassword) {
                            isHave = true
                            count = i
                        }
                    }

                } else Toast.makeText(
                    requireContext(),
                    "Iltimos malumotlatni to'ldiring",
                    Toast.LENGTH_SHORT
                ).show()
                if (isHave) {
                    findNavController().navigate(SingInFragmentDirections.actionSingInFragmentToUserFragment())


                    Toast.makeText(
                        requireContext(),
                        "SoftData ga xush kelibsiz ",
                        Toast.LENGTH_SHORT
                    ).show()
                    isHave = false
                } else Toast.makeText(
                    requireContext(),
                    "Malumotingizni qaytadan tekshiring",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }
        fragmentArgs.user?.let {
            with(binding) {
                etUsername.setText(it.userName)
                etPassword.setText(it.userPassword)
            }
        }
    }

    private fun getData(): MutableList<User> {
        userList.clear()
        referance.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {}

            override fun onDataChange(snapshot: DataSnapshot) {
                for (user in snapshot.children) {
                    val model = user.getValue(User::class.java)
                    userList.add(model as User)
                }

            }

        })
        return userList
    }

    private val fragmentArgs by lazy {
        navArgs<SingInFragmentArgs>().value
    }



}