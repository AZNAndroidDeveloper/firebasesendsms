package uz.app.loginscreen.presentation.singUpFragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.text.InputFilter
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import uz.app.loginscreen.R
import uz.app.loginscreen.data.model.User
import uz.app.loginscreen.databinding.FragmentSingUpBinding
import java.util.concurrent.TimeUnit

class SingUpFragment : Fragment(R.layout.fragment_sing_up) {
    private lateinit var binding: FragmentSingUpBinding
    private lateinit var database: FirebaseDatabase

    private lateinit var referance: DatabaseReference
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSingUpBinding.bind(view)
        database = FirebaseDatabase.getInstance()
//        permissionSms()
//        phone()
        referance = database.getReference("Users")
        with(binding) {
            loginPrew.apply {
                setOnClickListener {
                    setData()
                }

            }
        }
    }
//        requireActivity().onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(false) {
//            override fun handleOnBackPressed() {
//
//            }
//
//        })


    private fun setData(): Boolean {
        var isHave = false

        with(binding) {
            val username = etUserNameSingUp.text.toString().trim()
            val password = etSignUpPassword.text.toString().trim()
            val phone = etPhone.rawText.toString()
            val email = etEmail.text.toString()

            if (username.isNotEmpty() && isValidEmail() && password.isNotEmpty() && isValidPhone()) {
                val id = referance.push().key
                isHave = true

                if (id != null) {
                    referance.child(id).setValue(User(username, password, email, phone))
                    val testPhoneVerify = testPhoneVerify()
                    findNavController().navigate(
                        SingUpFragmentDirections.actionSingUpFragmentToSmsFragment(testPhoneVerify)
                    )

                } else
                    Toast.makeText(requireContext(), "Id is null", Toast.LENGTH_SHORT).show()
            } else
                Toast.makeText(
                    requireContext(),
                    "Iltimos malumotlarni toliq kiriting",
                    Toast.LENGTH_SHORT
                ).show()

        }
        return isHave
    }



    private fun testPhoneVerify():String {
        // [START auth_test_phone_verify]
        val phone = binding.etPhone.rawText
        val phoneNum = "+998$phone"
        var code = ""
        Log.d("TAG", "testPhoneVerify: $phoneNum")
        val testVerificationCode = "123456"

        // Whenever verification is triggered with the whitelisted number,
        // provided it is not set for auto-retrieval, onCodeSent will be triggered.
        val options = PhoneAuthOptions.newBuilder(Firebase.auth)
            .setPhoneNumber(phoneNum)
            .setTimeout(30L, TimeUnit.SECONDS)
            .setActivity(requireActivity())
            .setCallbacks(object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onCodeSent(
                    verificationId: String,
                    forceResendingToken: PhoneAuthProvider.ForceResendingToken
                ) {
                    // Save the verification id somewhere
                    // ...
                    Log.wtf(
                        "SignUpFragment",
                        "sign in with phone -> CODE SENT verificationId = $verificationId, forceResendingToken = $forceResendingToken"
                    )
                    // The corresponding whitelisted code above should be used to complete sign-in.
//                    requireContext().enableUserManuallyInputCode()
                    // FIXME: 12.02.2021   .enableUserManuallyInputCode() metodni ishlatib quyish kerak
                    // TODO: 12.02.2021  .enableUserManuallyInputCode() metodni ishlatib quyish kerak
                }

                override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                    // Sign in with the credential
                    // ...
                   code =  phoneAuthCredential.smsCode.toString()
                    Log.d("ab", "onViewCreated: $code")
                    Log.wtf(
                        "SignUpFragment",
                        "sign in with phone -> completed details = ${phoneAuthCredential.smsCode}"
                    )
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    // ...
                    Log.wtf("SingUpFragment", "sign in with phone -> failed error = $e")
                }
            })
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
        // [END auth_test_phone_verify]
        return code
    }

    private fun isValidEmail(): Boolean {
        var isEmail = false
        with(binding) {
            val email = etEmail.text.toString().trim()
            if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                isEmail = true
            } else {
                textInputThree.error = "Siz notog'ri email kiritingiz "
            }
        }
        return isEmail
    }

    fun isValidPhone(): Boolean {
        var isvalid = false
        with(binding) {
            Log.d("tag", "isValidPhone: ${etPhone.rawText.length}")
            if (etPhone.rawText.length==9) {
                isvalid = true
            } else {
                textInputFour.error = "No'merni to'liq kiriting"
            }
        }
        return isvalid
    }
}
